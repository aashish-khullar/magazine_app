class ArticlesController < ApplicationController
	def index
    @article = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
  end

  def create
    @article = Article.new
    @article.title = params[:article][:title]
    @article.body = params[:article][:body]
    if @article.save
      flash[:success] = "Article Created"
      redirect_to root_path
    else
      flash[:warning] = "Please Do not Submit Empty Article"
      render :new
    end
  end

end
