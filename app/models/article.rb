class Article < ApplicationRecord
	has_many :comments, as: :commentable
	validates_presence_of :body
	validates_presence_of :title
end
